create or replace package cursist.back_office is

   -- Author  : NICOJ
   -- Created : 16-11-2019 14:46:55
   -- Purpose : Back office Apex backend

   function error_handling(p_error in apex_error.t_error) return apex_error.t_error_result;

end back_office;
/
