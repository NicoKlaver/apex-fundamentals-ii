create or replace package cursist.front_office is
   -- Sub-Program Unit Declarations 
   function check_user(p_username in varchar2
                      ,p_password in varchar2) return boolean;
   function get_use_id(p_username in varchar2) return number;
   function page_valid(p_apex_id in number
                      ,p_usr_id  in number) return boolean;
   procedure post_authentication;
   function get_highlight_text(p_index  in varchar2
                              ,p_id     in varchar2
                              ,p_query  in varchar2
                              ,p_amount in number := 100) return varchar2;
end front_office;
/
