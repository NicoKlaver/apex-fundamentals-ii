create or replace package cursist.user_security is
   /**
   * Password security API
   *
   * @author Nico Klaver -  nklaver@itium.nl
   */

   /**
   * Returns the Hash for the Username/Password supplied.
   *
   * @param in_username Username
   * @param in_password Password
   *
   * @return Hash for the Username/Password supplied. 
   */
   function get_hash(in_username in varchar2
                    ,in_password in varchar2) return varchar2;

   /**
   * Returns TRUE when in_value1 and in_value2 are both nul or equal to each other.
   *
   * @param in_value1 Value to compare
   * @param in_value2 Value to compare
   */
   function is_equal(in_value1 in varchar2
                    ,in_value2 in varchar2) return boolean;
end user_security;
/
