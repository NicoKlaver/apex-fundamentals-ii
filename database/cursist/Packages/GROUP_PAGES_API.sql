create or replace package cursist.group_pages_api is
   /**
   * Table API for GROUP_TABLES
   *
   * @author Nico Klaver -  nklaver@itium.nl
   */

   /**
   * Returns a : separated list of all pages for the group given. 
   *
   * @param in_group_id Group t handle.
   *
   * @return The  : separated list of all pages 
   */
   function get(in_group_id in number) return varchar2;

   /**
   * Save the selected pages in GROUP_PAGES
   *
   * @param in_group_id Group to handle
   * @param in_pag      : separated list of all authorized tables.
   */
   procedure put(in_group_id in number
                ,in_pag      in varchar2);

end group_pages_api;
/
