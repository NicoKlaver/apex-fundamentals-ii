create or replace package body cursist.group_pages_api is
   /*
   * Table API for GROUP_TABLES
   */

   /**
   * Returns a : separated list of all pages for the group given. 
   *
   * @param in_group_id Group t handle.
   *
   * @return The  : separated list of all pages 
   */
   function get(in_group_id in number) return varchar2 is
      --
      -- lt_pag    Pages authorized for IN_GROUP_ID 
      lt_pag apex_t_number;
      l_list varchar2(4000);
   begin
      if in_group_id is not null
      then
         select gpg.page_id
         bulk   collect
         into   lt_pag
         from   group_pages gpg
         where  gpg.group_id = in_group_id;
         --
         -- Convert the selction to a : separated list.
         l_list := apex_string.join(p_table => lt_pag, p_sep => ':');
      end if;
      --
      return l_list;
   exception
      when others then
         raise;
   end get;
   /**
   * Save the selected pages in GROUP_PAGES
   *
   * @param in_group_id Group to handle
   * @param in_pag      : separated list of all authorized tables.
   */
   procedure put(in_group_id in number
                ,in_pag      in varchar2) is
      --
      -- lt_pag    Pages authorized for IN_GROUP_ID 
      lt_pag apex_t_number;
   begin
      if in_group_id is not null
      then
         delete from group_pages gpg
         where  gpg.group_id = in_group_id;
         --
         lt_pag := apex_string.split_numbers(p_str => in_pag, p_sep => ':');
         forall idx in 1 .. lt_pag.count
            insert into group_pages
               (group_id
               ,page_id)
            values
               (in_group_id
               ,lt_pag(idx));
      
      end if;
   exception
      when others then
         raise;
   end put;
end group_pages_api;
/
