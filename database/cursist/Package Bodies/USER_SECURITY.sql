create or replace package body cursist.user_security is
   /*
   * Password security API
   */

   /**
   * Returns the Hash for the Username/Password supplied.
   *
   * @param in_username Username
   * @param in_password Password
   *
   * @return Hash for the Username/Password supplied. 
   */
   function get_hash(in_username in varchar2
                    ,in_password in varchar2) return varchar2 is
      l_salt varchar2(30) := 'PutYourSaltHere';
      l_hash users.password%type;
   begin
      if in_username is not null and
         in_password is not null
      then
         l_hash := dbms_crypto.hash(utl_raw.cast_to_raw(upper(in_username) || l_salt || in_password), dbms_crypto.hash_sh256);
      end if;
      return l_hash;
   exception
      when others then
         raise;
   end get_hash;

   /**
   * Returns TRUE when in_value1 and in_value2 are both nul or equal to each other.
   *
   * @param in_value1 Value to compare
   * @param in_value2 Value to compare
   */
   function is_equal(in_value1 in varchar2
                    ,in_value2 in varchar2) return boolean is
      l_retval boolean;
   begin
      if in_value1 is null and
         in_value2 is null
      then
         l_retval := true;
      else
         l_retval := coalesce(in_value1 = in_value2, false);
      end if;
      
      return l_retval;
   end is_equal;
end user_security;
/
