create or replace package body cursist.front_office is
   -- Cursor Bodies 
   -- Sub-Program Units 
   function check_user(p_username in varchar2
                      ,p_password in varchar2) return boolean is
      -- Check user credentials 
   
      l_dummy number;
      l_hash  users.password%type;
   
   begin
      l_hash := user_security.get_hash(in_username => p_username, in_password => p_password);
      --
      select 1
      into   l_dummy
      from   users u
      where  upper(user_name) = upper(p_username)
      and    password = l_hash
      and    u.indication_active = 'Y';
   
      return true;
   
   exception
      when others then
         -- no_data_found or too_many_rows 
         return false;
      
   end;
   function get_use_id(p_username in varchar2) return number is
      -- get user id 
   
      l_use_id number;
   
   begin
   
      select id
      into   l_use_id
      from   users
      where  upper(user_name) = upper(p_username);
   
      return l_use_id;
   
   exception
      when others then
         -- no_data_found or too_many_rows 
         return 0;
      
   end;
   function page_valid(p_apex_id in number
                      ,p_usr_id  in number) return boolean is
      -- check if user has access to page 
   
      l_dummy number;
   
   begin
   
      select 1
      into   l_dummy
      from   users       usr
            ,group_pages gpa
            ,pages       pag
      where  usr.id = p_usr_id
      and    usr.group_id = gpa.group_id
      and    gpa.page_id = pag.id
      and    pag.apex_id = p_apex_id;
   
      return true;
   
   exception
      when no_data_found then
         return false;
      
   end;

   function get_highlight_text(p_index  in varchar2
                              ,p_id     in varchar2
                              ,p_query  in varchar2
                              ,p_amount in number := 100) return varchar2 is
      l_hitab         ctx_doc.highlight_tab;
      l_doc           clob;
      l_start         pls_integer;
      l_start_amount  pls_integer;
      l_return_string varchar2(30000);
   begin
      ctx_doc.highlight(p_index, p_id, p_query, l_hitab, true);
      ctx_doc.filter(p_index, p_id, l_doc, true);
      if l_hitab(1).offset > p_amount + 1
      then
         l_start        := l_hitab(1).offset - (p_amount + 1);
         l_start_amount := p_amount + 1;
      else
         l_start        := 1;
         l_start_amount := l_hitab(1).offset - 1;
      end if;
      l_return_string := '...' || dbms_lob.substr(l_doc, l_start_amount, l_start) || '<FONT style="BACKGROUND-COLOR: yellow; color:black" >' ||
                         dbms_lob.substr(l_doc, l_hitab(1).length, l_hitab(1).offset) || '</FONT>' ||
                         dbms_lob.substr(l_doc, p_amount, l_hitab(1).length + l_hitab(1).offset) || '...';
      return l_return_string;
   end;

   procedure post_authentication is
   begin
      apex_util.set_session_state(p_name => 'G_USER_ID', p_value => get_use_id(p_username => v('APP_USER')));
   end;
end front_office;
/
