import p15$module from "./js/p15";
import util$module from "./js/util";

var util;
var p1;

apex.jQuery(document).ready(() => {
	const pageId = Number(document.getElementById("pFlowStepId").value);

  switch (pageId) {
    case 1:

    case 15:
      p15$module.init(apex.debug);  
      util = util$module.init(apex.jQuery, apex.debug, apex.item);    
      break;
  
    default:
      apex.debug.log("default: " + pageId);
      break;
  }
});

export {
  util
}