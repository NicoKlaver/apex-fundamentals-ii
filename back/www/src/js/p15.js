/**
 * Initializes p15 module for APEX page 15.
 * 
 * @function init
 * 
 * @param {Object} debug APEX debug module
 **/
 const init = (debug) => {
   debug.log("p5.init");
 };
 
 export default {
   init
 };
 