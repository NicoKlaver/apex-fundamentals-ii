insert into documents
   (document
   ,date_created
   ,description
   ,filename
   ,mimetype
   ,last_update_date
   ,rating)
   select doc.document
         ,doc.date_created
         ,doc.description
         ,doc.filename
         ,doc.mimetype
         ,doc.last_update_date
         ,doc.rating
   from   documents doc;
