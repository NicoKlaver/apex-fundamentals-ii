set define '^'
set verify off
set serveroutput on size 1000000
set feedback off
WHENEVER SQLERROR EXIT SQL.SQLCODE ROLLBACK
 
prompt  Set Credentials...
 
begin
 
  -- Assumes you are running the script connected to sqlplus as the schema associated with the UI defaults or as the product schema.
  wwv_flow_api.set_security_group_id(p_security_group_id=>1700436502411350);
 
end;
/

begin wwv_flow.g_import_in_progress := true; end;
/
begin 

select value into wwv_flow_api.g_nls_numeric_chars from nls_session_parameters where parameter='NLS_NUMERIC_CHARACTERS';

end;

/
begin execute immediate 'alter session set nls_numeric_characters=''.,''';

end;

/
begin wwv_flow.g_browser_language := 'en'; end;
/
prompt  Check Compatibility...
 
begin
 
-- This date identifies the minimum version required to install this file.
wwv_flow_api.set_version(p_version_yyyy_mm_dd=>'2019.10.04');
 
end;
/

-- SET SCHEMA
 
begin
 
   wwv_flow_api.g_id_offset := 0;
   wwv_flow_hint.g_schema   := 'CURSIST';
   wwv_flow_hint.check_schema_privs;
 
end;
/

 
--------------------------------------------------------------------
prompt  SCHEMA CURSIST - User Interface Defaults, Table Defaults
--
-- Import using sqlplus as the Oracle user: APEX_190200
-- Exported 18:23 Monday March 9, 2020 by: ADMIN_CURSIST
--
 
begin
 
wwv_flow_hint.remove_hint_priv(wwv_flow_hint.g_schema,'ANNOUNCEMENTS');
wwv_flow_hint.create_table_hint_priv(
  p_table_id => 4163153460188352 + wwv_flow_api.g_id_offset,
  p_schema => wwv_flow_hint.g_schema,
  p_table_name  => 'ANNOUNCEMENTS',
  p_report_region_title => 'Announcements',
  p_form_region_title => 'Announcement Details');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Announcement Text',
  p_display_seq_form => 5,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_RICH_TEXT_EDITOR',
  p_form_attribute_02 => 'Basic',
  p_form_attribute_03 => 'Y',
  p_form_attribute_04 => 'moono',
  p_form_attribute_05 => 'top',
  p_display_as_tab_form => 'TEXTAREA',
  p_display_seq_report => 5,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'N',
  p_alignment => 'L',
  p_display_width => 60,
  p_max_width => 4000,
  p_height => 4,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4163708120188353 + wwv_flow_api.g_id_offset,
  p_table_id => 4163153460188352 + wwv_flow_api.g_id_offset,
  p_column_name => 'ANNOUNCEMENT_TEXT');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Group',
  p_display_seq_form => 3,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_SELECT_LIST',
  p_form_attribute_01 => 'NONE',
  p_form_attribute_02 => 'N',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 3,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_lov_query => 'SELECT gro.name AS d,'||chr(10)||
'       gro.id   AS r'||chr(10)||
'FROM   groups gro'||chr(10)||
'ORDER  BY 1'||chr(10)||
'',
  p_required => 'N',
  p_alignment => 'L',
  p_display_width => 30,
  p_max_width => 22,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'N',
  p_column_id => 4163489815188352 + wwv_flow_api.g_id_offset,
  p_table_id => 4163153460188352 + wwv_flow_api.g_id_offset,
  p_column_name => 'GROUP_ID');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Id',
  p_display_seq_form => 1,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_HIDDEN',
  p_form_attribute_01 => 'Y',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 1,
  p_display_in_report => 'N',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'Y',
  p_alignment => 'R',
  p_display_width => 30,
  p_max_width => 22,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'N',
  p_column_id => 4163234554188352 + wwv_flow_api.g_id_offset,
  p_table_id => 4163153460188352 + wwv_flow_api.g_id_offset,
  p_column_name => 'ID');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Active?',
  p_display_seq_form => 4,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_SELECT_LIST',
  p_form_attribute_01 => 'NONE',
  p_form_attribute_02 => 'N',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 4,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'Y',
  p_alignment => 'L',
  p_display_width => 30,
  p_max_width => 1,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4163591983188353 + wwv_flow_api.g_id_offset,
  p_table_id => 4163153460188352 + wwv_flow_api.g_id_offset,
  p_column_name => 'INDICATION_ACTIVE');
 
end;
/

 
begin
 
wwv_flow_hint.create_lov_data_priv(
  p_id => 4164901880307367 + wwv_flow_api.g_id_offset,
  p_column_id => 4163591983188353 + wwv_flow_api.g_id_offset,
  p_lov_disp_sequence => 1,
  p_lov_disp_value => 'Yes',
  p_lov_return_value => 'Y');
 
end;
/

 
begin
 
wwv_flow_hint.create_lov_data_priv(
  p_id => 4164938604307367 + wwv_flow_api.g_id_offset,
  p_column_id => 4163591983188353 + wwv_flow_api.g_id_offset,
  p_lov_disp_sequence => 2,
  p_lov_disp_value => 'No',
  p_lov_return_value => 'N');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'User',
  p_display_seq_form => 2,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_SELECT_LIST',
  p_form_attribute_01 => 'NONE',
  p_form_attribute_02 => 'N',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 2,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_lov_query => 'SELECT apex_string.format(p_message => ''%0 %1'', p0 => u.first_name, p1 => u.last_name) AS d,'||chr(10)||
'       u.id AS r'||chr(10)||
'FROM   users u'||chr(10)||
'ORDER  BY u.last_name,'||chr(10)||
'          u.first_name'||chr(10)||
'',
  p_required => 'N',
  p_alignment => 'R',
  p_display_width => 30,
  p_max_width => 22,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'N',
  p_column_id => 4163345210188352 + wwv_flow_api.g_id_offset,
  p_table_id => 4163153460188352 + wwv_flow_api.g_id_offset,
  p_column_name => 'USER_ID');
 
end;
/

 
begin
 
wwv_flow_hint.remove_hint_priv(wwv_flow_hint.g_schema,'GROUPS');
wwv_flow_hint.create_table_hint_priv(
  p_table_id => 4161391270188351 + wwv_flow_api.g_id_offset,
  p_schema => wwv_flow_hint.g_schema,
  p_table_name  => 'GROUPS',
  p_report_region_title => 'Groups',
  p_form_region_title => 'Group Details');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Description',
  p_display_seq_form => 3,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_TEXT_FIELD',
  p_form_attribute_01 => 'N',
  p_form_attribute_02 => 'N',
  p_form_attribute_04 => 'TEXT',
  p_form_attribute_05 => 'BOTH',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 3,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'N',
  p_alignment => 'L',
  p_display_width => 60,
  p_max_width => 4000,
  p_height => 4,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4161676360188351 + wwv_flow_api.g_id_offset,
  p_table_id => 4161391270188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'DESCRIPTION');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Id',
  p_display_seq_form => 1,
  p_display_in_form => 'N',
  p_display_as_form => 'NATIVE_HIDDEN',
  p_form_attribute_01 => 'Y',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 1,
  p_display_in_report => 'N',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'Y',
  p_alignment => 'R',
  p_display_width => 30,
  p_max_width => 22,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'N',
  p_column_id => 4161429939188351 + wwv_flow_api.g_id_offset,
  p_table_id => 4161391270188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'ID');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Name',
  p_display_seq_form => 2,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_TEXT_FIELD',
  p_form_attribute_01 => 'N',
  p_form_attribute_02 => 'N',
  p_form_attribute_04 => 'TEXT',
  p_form_attribute_05 => 'BOTH',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 2,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'N',
  p_alignment => 'L',
  p_display_width => 60,
  p_max_width => 255,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4161606008188351 + wwv_flow_api.g_id_offset,
  p_table_id => 4161391270188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'NAME');
 
end;
/

 
begin
 
wwv_flow_hint.remove_hint_priv(wwv_flow_hint.g_schema,'GROUP_PAGES');
wwv_flow_hint.create_table_hint_priv(
  p_table_id => 4163797450188353 + wwv_flow_api.g_id_offset,
  p_schema => wwv_flow_hint.g_schema,
  p_table_name  => 'GROUP_PAGES',
  p_report_region_title => 'Group Pages',
  p_form_region_title => 'Group Pages');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Group',
  p_display_seq_form => 2,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_SELECT_LIST',
  p_form_attribute_01 => 'NONE',
  p_form_attribute_02 => 'N',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 2,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_lov_query => 'SELECT gro.name AS d,'||chr(10)||
'       gro.id   AS r'||chr(10)||
'FROM   groups gro'||chr(10)||
'ORDER  BY 1'||chr(10)||
'',
  p_required => 'N',
  p_alignment => 'R',
  p_display_width => 30,
  p_max_width => 22,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'N',
  p_column_id => 4164012106188353 + wwv_flow_api.g_id_offset,
  p_table_id => 4163797450188353 + wwv_flow_api.g_id_offset,
  p_column_name => 'GROUP_ID');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Id',
  p_display_seq_form => 1,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_HIDDEN',
  p_form_attribute_01 => 'Y',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 1,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'Y',
  p_alignment => 'R',
  p_display_width => 30,
  p_max_width => 22,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'N',
  p_column_id => 4163897725188353 + wwv_flow_api.g_id_offset,
  p_table_id => 4163797450188353 + wwv_flow_api.g_id_offset,
  p_column_name => 'ID');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Page',
  p_display_seq_form => 3,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_SELECT_LIST',
  p_form_attribute_01 => 'NONE',
  p_form_attribute_02 => 'N',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 3,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_lov_query => 'SELECT apex_string.format(p_message => ''%0 (%1)'', p0 => pag.apex_id, p1 => pag.page_name) AS d,'||chr(10)||
'       pag.id AS r'||chr(10)||
'FROM   pages pag'||chr(10)||
'ORDER  BY 1'||chr(10)||
'',
  p_required => 'Y',
  p_alignment => 'L',
  p_display_width => 30,
  p_max_width => 22,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'N',
  p_column_id => 4164025800188353 + wwv_flow_api.g_id_offset,
  p_table_id => 4163797450188353 + wwv_flow_api.g_id_offset,
  p_column_name => 'PAGE_ID');
 
end;
/

 
begin
 
wwv_flow_hint.remove_hint_priv(wwv_flow_hint.g_schema,'PAGES');
wwv_flow_hint.create_table_hint_priv(
  p_table_id => 4160896314188303 + wwv_flow_api.g_id_offset,
  p_schema => wwv_flow_hint.g_schema,
  p_table_name  => 'PAGES',
  p_report_region_title => 'Pages',
  p_form_region_title => 'Page Details');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Apex Page Id',
  p_display_seq_form => 2,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_NUMBER_FIELD',
  p_form_attribute_03 => 'left',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 2,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'Y',
  p_alignment => 'L',
  p_display_width => 30,
  p_max_width => 22,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'N',
  p_column_id => 4161110734188351 + wwv_flow_api.g_id_offset,
  p_table_id => 4160896314188303 + wwv_flow_api.g_id_offset,
  p_column_name => 'APEX_ID');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Id',
  p_display_seq_form => 1,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_HIDDEN',
  p_form_attribute_01 => 'Y',
  p_display_as_tab_form => 'HIDDEN',
  p_display_seq_report => 1,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'Y',
  p_alignment => 'L',
  p_display_width => 30,
  p_max_width => 22,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'N',
  p_column_id => 4160922334188350 + wwv_flow_api.g_id_offset,
  p_table_id => 4160896314188303 + wwv_flow_api.g_id_offset,
  p_column_name => 'ID');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Page Name',
  p_display_seq_form => 3,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_TEXT_FIELD',
  p_form_attribute_01 => 'N',
  p_form_attribute_02 => 'N',
  p_form_attribute_04 => 'TEXT',
  p_form_attribute_05 => 'BOTH',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 3,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'Y',
  p_alignment => 'L',
  p_display_width => 60,
  p_max_width => 255,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4161212205188351 + wwv_flow_api.g_id_offset,
  p_table_id => 4160896314188303 + wwv_flow_api.g_id_offset,
  p_column_name => 'PAGE_NAME');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Page Title',
  p_display_seq_form => 4,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_TEXT_FIELD',
  p_form_attribute_01 => 'N',
  p_form_attribute_02 => 'N',
  p_form_attribute_04 => 'TEXT',
  p_form_attribute_05 => 'BOTH',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 4,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'N',
  p_alignment => 'L',
  p_display_width => 60,
  p_max_width => 255,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4161287192188351 + wwv_flow_api.g_id_offset,
  p_table_id => 4160896314188303 + wwv_flow_api.g_id_offset,
  p_column_name => 'PAGE_TITLE');
 
end;
/

 
begin
 
wwv_flow_hint.remove_hint_priv(wwv_flow_hint.g_schema,'USERS');
wwv_flow_hint.create_table_hint_priv(
  p_table_id => 4161735220188351 + wwv_flow_api.g_id_offset,
  p_schema => wwv_flow_hint.g_schema,
  p_table_name  => 'USERS',
  p_report_region_title => 'Users',
  p_form_region_title => 'User Details');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Address Line 1',
  p_display_seq_form => 50,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_TEXT_FIELD',
  p_form_attribute_01 => 'N',
  p_form_attribute_02 => 'N',
  p_form_attribute_04 => 'TEXT',
  p_form_attribute_05 => 'BOTH',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 50,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'Y',
  p_alignment => 'L',
  p_display_width => 60,
  p_max_width => 100,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4162252095188352 + wwv_flow_api.g_id_offset,
  p_table_id => 4161735220188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'ADDRESS_LINE_1');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Address Line 2',
  p_display_seq_form => 60,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_TEXT_FIELD',
  p_form_attribute_01 => 'N',
  p_form_attribute_02 => 'N',
  p_form_attribute_04 => 'TEXT',
  p_form_attribute_05 => 'BOTH',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 60,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'N',
  p_alignment => 'L',
  p_display_width => 60,
  p_max_width => 100,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4162347830188352 + wwv_flow_api.g_id_offset,
  p_table_id => 4161735220188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'ADDRESS_LINE_2');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Address Line 3',
  p_display_seq_form => 70,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_TEXT_FIELD',
  p_form_attribute_01 => 'N',
  p_form_attribute_02 => 'N',
  p_form_attribute_04 => 'TEXT',
  p_form_attribute_05 => 'BOTH',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 70,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'N',
  p_alignment => 'L',
  p_display_width => 60,
  p_max_width => 100,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4162497392188352 + wwv_flow_api.g_id_offset,
  p_table_id => 4161735220188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'ADDRESS_LINE_3');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Email',
  p_display_seq_form => 110,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_TEXT_FIELD',
  p_form_attribute_01 => 'N',
  p_form_attribute_02 => 'N',
  p_form_attribute_04 => 'TEXT',
  p_form_attribute_05 => 'BOTH',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 110,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'N',
  p_alignment => 'L',
  p_display_width => 60,
  p_max_width => 50,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4162823130188352 + wwv_flow_api.g_id_offset,
  p_table_id => 4161735220188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'EMAIL');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'First Name',
  p_display_seq_form => 30,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_TEXT_FIELD',
  p_form_attribute_01 => 'N',
  p_form_attribute_02 => 'N',
  p_form_attribute_04 => 'TEXT',
  p_form_attribute_05 => 'BOTH',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 30,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'Y',
  p_alignment => 'L',
  p_display_width => 60,
  p_max_width => 50,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4162024668188352 + wwv_flow_api.g_id_offset,
  p_table_id => 4161735220188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'FIRST_NAME');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Group ',
  p_help_text => 'Group where this user belongs to',
  p_display_seq_form => 15,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_SELECT_LIST',
  p_form_attribute_01 => 'NONE',
  p_form_attribute_02 => 'N',
  p_display_as_tab_form => 'TEXT_FROM_LOV',
  p_display_seq_report => 15,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_lov_query => 'SELECT gro.name AS d,'||chr(10)||
'       gro.id   AS r'||chr(10)||
'FROM   groups gro'||chr(10)||
'ORDER  BY 1'||chr(10)||
'',
  p_required => 'Y',
  p_alignment => 'L',
  p_display_width => 30,
  p_max_width => 22,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'N',
  p_column_id => 4161924148188352 + wwv_flow_api.g_id_offset,
  p_table_id => 4161735220188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'GROUP_ID');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Id',
  p_display_seq_form => 1,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_HIDDEN',
  p_form_attribute_01 => 'Y',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 1,
  p_display_in_report => 'N',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'Y',
  p_alignment => 'R',
  p_display_width => 30,
  p_max_width => 22,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'N',
  p_column_id => 4161874802188351 + wwv_flow_api.g_id_offset,
  p_table_id => 4161735220188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'ID');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Active?',
  p_display_seq_form => 10,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_SELECT_LIST',
  p_form_attribute_01 => 'NONE',
  p_form_attribute_02 => 'N',
  p_display_as_tab_form => 'SELECT_LIST_FROM_LOV',
  p_display_seq_report => 10,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'Y',
  p_alignment => 'L',
  p_display_width => 30,
  p_max_width => 1,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4162549650188352 + wwv_flow_api.g_id_offset,
  p_table_id => 4161735220188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'INDICATION_ACTIVE');
 
end;
/

 
begin
 
wwv_flow_hint.create_lov_data_priv(
  p_id => 4164582333270386 + wwv_flow_api.g_id_offset,
  p_column_id => 4162549650188352 + wwv_flow_api.g_id_offset,
  p_lov_disp_sequence => 1,
  p_lov_disp_value => 'Yes',
  p_lov_return_value => 'Y');
 
end;
/

 
begin
 
wwv_flow_hint.create_lov_data_priv(
  p_id => 4164657915270386 + wwv_flow_api.g_id_offset,
  p_column_id => 4162549650188352 + wwv_flow_api.g_id_offset,
  p_lov_disp_sequence => 2,
  p_lov_disp_value => 'No',
  p_lov_return_value => 'N');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Last Name',
  p_display_seq_form => 40,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_TEXT_FIELD',
  p_form_attribute_01 => 'N',
  p_form_attribute_02 => 'N',
  p_form_attribute_04 => 'TEXT',
  p_form_attribute_05 => 'BOTH',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 40,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'Y',
  p_alignment => 'L',
  p_display_width => 60,
  p_max_width => 50,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4162160648188352 + wwv_flow_api.g_id_offset,
  p_table_id => 4161735220188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'LAST_NAME');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Mobile Phone Nr',
  p_display_seq_form => 100,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_TEXT_FIELD',
  p_form_attribute_01 => 'N',
  p_form_attribute_02 => 'N',
  p_form_attribute_04 => 'TEXT',
  p_form_attribute_05 => 'BOTH',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 100,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'N',
  p_alignment => 'L',
  p_display_width => 30,
  p_max_width => 25,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4162723207188352 + wwv_flow_api.g_id_offset,
  p_table_id => 4161735220188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'MOBILE_PHONE_NR');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Password',
  p_display_seq_form => 130,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_PASSWORD',
  p_form_attribute_01 => 'N',
  p_display_as_tab_form => 'TEXTAREA',
  p_display_seq_report => 130,
  p_display_in_report => 'N',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'Y',
  p_alignment => 'L',
  p_display_width => 60,
  p_max_width => 4000,
  p_height => 4,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4163025253188352 + wwv_flow_api.g_id_offset,
  p_table_id => 4161735220188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'PASSWORD');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'Telephone Nr',
  p_display_seq_form => 90,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_TEXT_FIELD',
  p_form_attribute_01 => 'N',
  p_form_attribute_02 => 'N',
  p_form_attribute_04 => 'TEXT',
  p_form_attribute_05 => 'BOTH',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 90,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'N',
  p_alignment => 'L',
  p_display_width => 30,
  p_max_width => 25,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4162693529188352 + wwv_flow_api.g_id_offset,
  p_table_id => 4161735220188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'TELEPHONE_NR');
 
end;
/

 
begin
 
wwv_flow_hint.create_column_hint_priv(
  p_label => 'User Name',
  p_display_seq_form => 120,
  p_display_in_form => 'Y',
  p_display_as_form => 'NATIVE_TEXT_FIELD',
  p_form_attribute_01 => 'N',
  p_form_attribute_02 => 'N',
  p_form_attribute_04 => 'TEXT',
  p_form_attribute_05 => 'BOTH',
  p_display_as_tab_form => 'TEXT',
  p_display_seq_report => 120,
  p_display_in_report => 'Y',
  p_display_as_report => 'ESCAPE_SC',
  p_aggregate_by => 'N',
  p_required => 'Y',
  p_alignment => 'L',
  p_display_width => 60,
  p_max_width => 255,
  p_height => 1,
  p_group_by => 'N',
  p_searchable => 'Y',
  p_column_id => 4162939334188352 + wwv_flow_api.g_id_offset,
  p_table_id => 4161735220188351 + wwv_flow_api.g_id_offset,
  p_column_name => 'USER_NAME');
 
end;
/

commit;
begin 
execute immediate 'alter session set nls_numeric_characters='''||wwv_flow_api.g_nls_numeric_chars||'''';
end;
/
set verify on
set feedback on
prompt  ...done
